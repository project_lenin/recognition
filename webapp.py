## import lib
import cv2
import boto3
import pusher
import emotions_recognition
import datetime
import time

#declared variables
channels_client = pusher.Pusher(app_id='896516',key='b35fe34c9af5d2e6af93',secret='53cd0bb1f23df86e1720',cluster='us2',ssl=True)
s3 = boto3.resource('s3')
client = boto3.client('rekognition', 'us-east-2')
bucketin = 'logistic-customer'
my_bucket = s3.Bucket(bucketin)


while(True):
    print("start of process")
    channels = channels_client.channels_info(u"presence-", [u'user_count'])

    face_cascade = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
    cap = cv2.VideoCapture(0)
    cap.set(3, 640)
    cap.set(4, 480)
    ret, img = cap.read()
    gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    faces = face_cascade.detectMultiScale(gray, 1.3, 5)

    if len(faces)==0: # to check if a face was caught
        print("##### I don't get the face.#####")
        emotion='unknown'
        person='unknown'
        similarity=0
        message='it was not possible to detect his face'
    else:
        print("#### start of the recognition process ####")
        cap.release() # turn off camera
        #send pusher notification
        messagee = {'indicator':'0','message':'taking a photo'}
        channels_client.trigger('my-channel', 'my-event', messagee)

        for (x,y,w,h) in faces:
            roi_gray = gray[y:y+h, x:x+w]
            roi_color = img[y:y+h, x:x+w]
            cv2.imwrite(str(w) + str(h) + '_faces.jpg', roi_color)
            cv2.rectangle(img,(x,y),(x+w,y+h),(255,0,0),2)

            sourceFile=str(w) + str(h) + '_faces.jpg'
            client_bucket = boto3.client('s3')
            #we declare response variables
            similarity = 0
            person = 'unknown'
            emotion = emotions_recognition.getEmotion(sourceFile)
            message = 'It was not possible to detect his name'

            #we obtain the listing of photos of amazon
            for key in client_bucket.list_objects( Bucket='logistic-customer')['Contents']:
                imageSource=open(sourceFile,'rb')
                imageSource2=imageSource.read()
                fileName = key['Key']
                #we compare photo taken with the ones stored in amazon
                print(sourceFile)
                response=client.compare_faces(SimilarityThreshold=70,
                                                  SourceImage={'Bytes': imageSource2},
                                                  TargetImage={'S3Object':{'Bucket':bucketin,'Name':fileName}}
                                                )
                # if there was a match, filled the varibles for the answer
                if response['FaceMatches']:
                      similarity =response['FaceMatches'][0]['Similarity']
                      person = fileName
                      emotion = emotions_recognition.getEmotion(sourceFile)
                      message = 'his name was detected'
                      break

            print("### end of the recognition process ###")
            #close photo
            imageSource.close()
            #send notification a pusher of the response
            messagee = {'indicator':'1','similarity': similarity,'person':person,'emotion':emotion,'message':message}
            channels_client.trigger('my-channel', 'my-event', messagee)
            channels = channels_client.channels_info(u"presence-", [u'user_count'])



#         #cv2.destroyAllWindows()
