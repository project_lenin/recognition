import boto3
import os
from pprint import pprint

client = boto3.client('rekognition', 'us-east-2')

def getEmotion(image) :
    #Reconocemos emociones
    imageSource=open(image,'rb')
    resp = client.detect_faces(Image={'Bytes':  imageSource.read()}, Attributes=['ALL'])
    #print(len(resp))

    for face in resp.get('FaceDetails'):

        w = face.get('BoundingBox').get('Width')
        h = face.get('BoundingBox').get('Height')
        l = face.get('BoundingBox').get('Left')
        t = face.get('BoundingBox').get('Top')

        #pprint(face)
        #x1 = image.size[0]*l
        #y1 = image.size[1]*t

        #x2 = x1+image.size[0]*w
        #y2 = y1+image.size[1]*h

        dict = {}
        for emotion in face.get('Emotions'):
            dict[float(emotion.get('Confidence'))] = emotion.get('Type')
        #ordenar
        clave_mayor = max(dict)
        #retorna la mayor emocion
        return dict.get(clave_mayor)
